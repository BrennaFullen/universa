import * as assert from 'assert';
import { UMI } from "../src/umi";

describe('Transport', () => {
  it('should instantiate a UMI instance', () => {
    const transport = new UMI()
    assert.ok(transport instanceof UMI)
  })

  it('should return the same instance from `instance`', () => {
    const transport1 = UMI.instance
    const transport2 = UMI.instance
    assert.strictEqual(transport1, transport2)
  })
})

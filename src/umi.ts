import { ChildProcess, spawn } from 'child_process';
import * as path from 'path';

const instance = Symbol('instance');
const proc = Symbol('proc');
const calls = Symbol('calls');

export class UMI {
  private static binaryPath = path.resolve(__dirname, '..', 'contrib', 'umi', 'bin', 'umi');
  private static [instance]: UMI;
  private [proc]: ChildProcess;
  private [calls]: Map<number, any>;
  private serial: number;

  static get instance(): UMI {
    if (!UMI[instance]) {
      UMI[instance] = new UMI();
    }
    return UMI[instance];
  }

  constructor() {
    this[calls] = new Map();
    this.serial = 0;
  }

  public async version() {
    const { version } = await this.call('version');
    return version;
  }

  public async instantiate(objectType: string, ...args: any[]) {
    return this.call('instantiate', { args: [objectType, ...args] });
  }

  public async invoke(remoteObjectId: string, methodName: string, ...args: any[]) {
    return this.call('invoke', { args: [remoteObjectId, methodName, ...args] });
  }

  public async invokeStatic(objectType: string, methodName: string, ...args: any[]) {
    return this.call('invoke', { args: [objectType, methodName, ...args] });
  }

  public async get(remoteObjectId: number) {
    return this.call('get', { args: [remoteObjectId] });
  }

  get transport(): ChildProcess {
    if (!this[proc]) {
      const child = spawn(UMI.binaryPath);
      child.stdout!.on('data', this.handleResponse.bind(this));
      child.on('exit', (code, signal) => {
        /// TODO: Check code and signal
        delete this[proc];
        this.serial = 0;
      });
      this[proc] = child;
    }

    return this[proc];
  }

  private handleResponse(chunk: Buffer) {
    const response = JSON.parse(chunk.toString());
    const { resolve, reject } = this[calls].get(response.ref);

    if (response.error) {
      reject(new Error(response.error.text));
    } else {
      resolve(response.result);
    }
  }

  private async call(name: string, args: any = {}): Promise<any> {
    const serial = this.serial++;

    return new Promise((resolve, reject) => {
      this[calls].set(serial, { resolve, reject });

      const cmd = JSON.stringify({ serial, cmd: name, ...args });
      this.transport.stdin!.write(`${cmd}\n`);
    }).finally(() => {
      this[calls].delete(serial);
    });
  }
}
